## Source code

You can clone the project from the bitbucket repository

### `git clone https://henryvyh@bitbucket.org/henryvyh/test-app.git`

## Available Scripts

In the project directory, you can run:

### `npm install`

After install dependences, you can run.

### `npx react-native run-android && npx react-native start`

**Note: In the resource directory, you'll find a debugging app for testing in your phone, app-debug.apk!**
