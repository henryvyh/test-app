export const COLORS = {
  primary: '#08cb8c',
  home: '#f30568',
  accent: '#f30568',
  badge: '#eb0a5d',
  card: '#e1e1e1',
  heart: '#f10768',
  btn: '#0797eb',
  textDark: '#444444',
  white: '#fff',
};
