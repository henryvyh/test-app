import moment from 'moment';
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {COLORS} from '../assets/colors';
import {Column} from '../styled/views';
import {getIcon} from '../utils/utils';
var days = {
  0: 'Today',
  '-1': 'Yesterday',
  1: 'Tomorrow',
};
const getTextDate = d => {
  var startOfToday = moment().startOf('day');
  var startOfDate = moment.unix(d).startOf('day');
  var daysDiff = startOfDate.diff(startOfToday, 'days');
  return Math.abs(daysDiff) <= 1
    ? days[daysDiff]
    : moment.unix(d).format('dddd');
};

const CardDay = ({data}) => {
  let dateText = getTextDate(data.dt);

  return (
    <Column style={styles.card}>
      <Text style={styles.day}>{dateText}</Text>
      <Text>{moment.unix(data?.dt).format('DD/M')}</Text>
      <Text style={styles.icon}>{getIcon(data?.weather[0].icon)}</Text>
      <Text>{data?.weather[0].main}</Text>
      <Text>
        <Text style={styles.bold}>T. min:</Text> {Math.round(data?.temp?.min)}°/{' '}
        <Text style={styles.bold}>T. max:</Text> {Math.round(data?.temp?.max)}°
      </Text>
    </Column>
  );
};

export default CardDay;
const styles = StyleSheet.create({
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 8,
    marginVertical: 8,
    backgroundColor: COLORS.white,
  },
  icon: {
    fontSize: 40,
    fontFamily: 'WeatherIcons-Regular',
    padding: 0,
    textAlign: 'center',
  },
  day: {
    fontWeight: 'bold',
  },
  hour: {
    fontSize: 11,
  },
  bold: {
    fontWeight: 'bold',
  },
  temp: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  windText: {
    fontSize: 11,
  },
  wind: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    alignContent: 'center',
  },
  iconWind: {
    marginRight: 4,
  },
});
