import React from 'react';
import {StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import {COLORS} from '../assets/colors';

const ButtonCompare = ({onPress, loading}) => {
  return (
    <Button
      loading={loading}
      icon="check"
      onPress={() => !loading && onPress()}
      style={styles.btn}
      mode="outlined">
      Rerserva ahoora
    </Button>
  );
};

export default ButtonCompare;

const styles = StyleSheet.create({
  btn: {
    backgroundColor: COLORS.white,
    borderWidth: 3,
    borderRadius: 8,
    marginHorizontal: 32,
    marginVertical: 16,
    borderColor: COLORS.primary,
  },
});
