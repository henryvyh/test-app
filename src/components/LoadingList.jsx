import React, {useCallback} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import ContentLoader from 'react-native-easy-content-loader';

const LoadingList = () => {
  const renderItem = useCallback(({item}) => (
    <ContentLoader aSize={40} active avatar pRows={2} pWidth={'100%'} />
  ));
  return (
    <FlatList
      data={Array.from(Array(10)).map((a, id) => ({id}))}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ItemSeparatorComponent={() => <View style={{height: 16}} />}
      style={styles.list}
    />
  );
};

export default LoadingList;
const styles = StyleSheet.create({
  list: {
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
});
