import moment from 'moment';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Chip, Divider, List} from 'react-native-paper';
import {COLORS} from '../assets/colors';
import {Absolute, Relative, RowCenter, Separator} from '../styled/views';
import {getIcon} from '../utils/utils';

const CardDetail = ({data}) => {
  return (
    <View style={styles.viewTempWrap}>
      <RowCenter>
        <Chip icon="weather-night-partly-cloudy">
          Amanecer {moment.unix(data?.sunrise).format('HH:mm')}
        </Chip>
        <Separator width="8" />
        <Chip icon="weather-night">
          Anochecer {moment.unix(data?.sunset).format('HH:mm')}
        </Chip>
      </RowCenter>
      <Divider style={styles.divider} />
      <View style={styles.viewTemp}>
        <View style={styles.item}>
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Sensación real"
            description={data?.feels_like + '°C'}
          />
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Probabilidad de lluvia"
            description={`${data?.rain ?? 0}%`}
          />
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Velocidad del viento"
            description={data?.wind_speed + 'km/h'}
          />
        </View>
        <View style={[styles.item, styles.center]}>
          <Text style={styles.textTempCurrent} numberOfLines={1}>
            {Math.round(data?.temp)}°
          </Text>
          <Text style={styles.hour}>{data?.weather[0].main}</Text>
          <Text style={[styles.icon, styles.iconCurrent]}>
            {getIcon(data?.weather[0].icon)}
          </Text>
        </View>
        <View style={styles.item}>
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Humedad"
            description={data?.humidity + '%'}
          />
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Presión"
            description={data?.pressure + 'mbar'}
          />
          <List.Item
            titleNumberOfLines={2}
            style={styles.list}
            title="Índice UV"
            description={data?.uvi}
          />
        </View>
      </View>
    </View>
  );
};

export default CardDetail;
const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  item: {
    flex: 1,
    flexGrow: 1,
  },
  viewTempWrap: {
    backgroundColor: '#fff',
    elevation: 1,
    marginTop: 8,
    paddingVertical: 8,
    borderRadius: 8,
    paddingHorizontal: 4,
  },
  viewTemp: {
    width: '100%',
    flexDirection: 'row',
  },
  textTempCurrent: {
    fontSize: 70,
    fontWeight: 'bold',
  },
  center: {
    alignItems: 'center',
  },
  hour: {
    fontSize: 20,
    marginHorizontal: 8,
  },
  iconCurrent: {
    fontSize: 90,
  },
  icon: {
    color: '#88888845',
    fontSize: 40,
    fontFamily: 'WeatherIcons-Regular',
    padding: 0,
  },
  list: {
    flex: 1,
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginVertical: 0,
  },
  divider: {
    marginVertical: 6,
  },
});
