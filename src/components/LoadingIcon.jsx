import React from 'react';
import {ActivityIndicator} from 'react-native';
import {COLORS} from '../assets/colors';
const LoadingIcon = ({size = 64}) => {
  return (
    <ActivityIndicator animating={true} size={size} color={COLORS.primary} />
  );
};

export default LoadingIcon;
