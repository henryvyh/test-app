import React from 'react';
import {Dimensions, StyleSheet, Text} from 'react-native';
import {Page} from '../styled/views';
import LottieView from 'lottie-react-native';
import {Button} from 'react-native-paper';
import {COLORS} from '../assets/colors';
const size = Dimensions.get('window');

const MessageView = ({icon, title, message, action, query, actionText}) => {
  let t = query ? `No se econtraron resultados` : title;
  let m = query
    ? `Intenta usar otra palabra clave o intentalo más tarde.`
    : message;
  return (
    <Page style={styles.wrapper}>
      {icon && (
        <LottieView
          style={{height: size.height * 0.3}}
          source={icon}
          autoPlay
          loop
        />
      )}
      <Text style={styles.title}>{t}</Text>
      <Text style={styles.subtitle}>{m}</Text>
      {action && (
        <Button
          color={COLORS.primary}
          onPress={action}
          mode="outlined"
          style={styles.btn}>
          {actionText}
        </Button>
      )}
    </Page>
  );
};

export default MessageView;

const styles = StyleSheet.create({
  wrapper: {
    padding: 32,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  subtitle: {
    textAlign: 'center',
    fontSize: 14,
  },
  btn: {
    borderRadius: 20,
    borderWidth: 2,
    borderColor: COLORS.primary,
    marginTop: 16,
  },
});
