import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import {
  Avatar,
  Button,
  Card,
  Checkbox,
  IconButton,
  Paragraph,
  Title,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../assets/colors';
import {ColumnCenter} from '../styled/views';

const CardPlace = memo(({data, filter, onPress}) => {
  const {display, city_name, state, country} = data;
  return (
    <Card style={styles.card} onPress={() => onPress(data)}>
      <Card.Title
        title={city_name + ' - ' + filter?.label}
        subtitle={country + ' - ' + state}
        left={props => <Avatar.Icon {...props} icon={filter?.icon} />}
        right={props => (
          <ColumnCenter>
            <Icon name={'chevron-right'} size={36} />
          </ColumnCenter>
        )}
      />
    </Card>
  );
});

export default CardPlace;
const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    elevation: 1,
    marginVertical: 2,
    marginHorizontal: 4,
    paddingRight: 6,
    paddingVertical: 2,
  },
  directions: {
    margin: 0,
  },
});
