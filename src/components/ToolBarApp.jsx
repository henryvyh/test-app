import React from 'react';
import {StyleSheet} from 'react-native';
import {Appbar} from 'react-native-paper';
import {COLORS} from '../assets/colors';
import {openLink} from '../utils/utils';

const ToolBarApp = () => {
  return (
    <Appbar dark={true} style={styles.bottom}>
      <Appbar.Content title="React Native" subtitle="Test app" />
      <Appbar.Action
        icon="source-branch"
        onPress={() =>
          openLink('https://bitbucket.org/henryvyh/test-app/src/master/')
        }
      />
      <Appbar.Action
        icon="dots-vertical"
        onPress={() => console.log('Pressed mail')}
      />
    </Appbar>
  );
};

export default ToolBarApp;
const styles = StyleSheet.create({});
