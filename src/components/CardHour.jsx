import moment from 'moment';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {COLORS} from '../assets/colors';
import {Column, Row} from '../styled/views';
import {getIcon} from '../utils/utils';

const CardHour = ({data}) => {
  return (
    <Column style={styles.card}>
      <Text style={styles.hour}>{moment.unix(data?.dt).format('HH:mm')}</Text>
      <Text style={styles.temp}>{Math.round(data?.temp)}°</Text>
      <Text style={styles.icon}>{getIcon(data.weather[0].icon)}</Text>
      <Row style={styles.wind}>
        <View
          style={[
            styles.iconWind,
            {transform: [{rotate: data.wind_deg + 'deg'}]},
          ]}>
          <Icon name="navigation-outline" size={16} />
        </View>
        <Text style={styles.windText}>{data?.wind_speed} km/h</Text>
      </Row>
    </Column>
  );
};

export default CardHour;
const styles = StyleSheet.create({
  card: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 8,
    marginVertical: 8,
    backgroundColor: COLORS.white,
  },
  icon: {
    fontSize: 30,
    fontFamily: 'WeatherIcons-Regular',
    padding: 0,
    textAlign: 'center',
  },
  hour: {
    fontSize: 11,
  },
  temp: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  windText: {
    fontSize: 11,
  },
  wind: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    alignContent: 'center',
  },
  iconWind: {
    marginRight: 4,
  },
});
