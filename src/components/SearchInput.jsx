import React, {useState, useRef, useEffect, useMemo} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {
  Button,
  Divider,
  IconButton,
  Menu,
  TouchableRipple,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import debounce from 'lodash.debounce';
import {COLORS} from '../assets/colors';
import {filtersType} from '../redux/place/PlaceReducer';

const SearchInput = ({
  onSearch,
  defaultValue = '',
  filter,
  onFilter,
  placeholder,
}) => {
  const inputRef = useRef(null);
  const [query, setQuery] = useState(defaultValue);
  const handleChange = e => {
    setQuery(e);
    onSearch?.(e);
  };
  const debouncedResults = useMemo(() => {
    return debounce(handleChange, 500);
  }, []);

  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  const onMenu = e => {
    onFilter(e);
    closeMenu();
  };
  return (
    <View style={[styles.wrapper]}>
      <View style={[styles.search]}>
        <Icon name="search" size={18} color={COLORS.primary} />
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <TouchableRipple
              style={styles.menu}
              rippleColor="rgba(0, 0, 0, .32)"
              onPress={openMenu}>
              <View style={styles.menuText}>
                <Text style={styles.text}>{filter?.label}</Text>
                <IconM name="menu-down-outline" size={18} />
              </View>
            </TouchableRipple>
          }>
          {filtersType?.map(e => (
            <Menu.Item key={e.id} onPress={() => onMenu(e)} title={e?.label} />
          ))}
        </Menu>
        <TextInput
          ref={inputRef}
          style={[styles.input]}
          placeholder={placeholder ?? '¿Qué estás buscando?'}
          defaultValue={defaultValue}
          onChangeText={debouncedResults}
        />
        {query ? (
          <IconButton
            icon="close-circle"
            color={COLORS.primary}
            size={22}
            style={styles.btnClear}
            onPress={() => {
              inputRef.current.setNativeProps({text: ''});
              debouncedResults('');
            }}
          />
        ) : null}
      </View>
    </View>
  );
};

export default SearchInput;

const styles = StyleSheet.create({
  wrapper: {
    // paddingBottom: 6,
    backgroundColor: COLORS.primary,
    // height: 38,
    width: '100%',
    paddingBottom: 8,
    paddingHorizontal: 16,
    // flex: 1,
    // flexGrow: 1,
  },
  search: {
    width: '100%',
    height: 36,
    borderRadius: 18,
    backgroundColor: 'white',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    paddingLeft: 12,
    paddingRight: 6,
    paddingVertical: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  input: {
    paddingHorizontal: 4,
    paddingLeft: 6,
    flexGrow: 1,
    fontWeight: 'bold',
    flex: 1,
    paddingVertical: 0,
  },
  btnClear: {
    marginRight: 0,
  },
  menu: {
    marginLeft: 4,
  },
  menuText: {
    flexDirection: 'row',
    borderRadius: 16,
    backgroundColor: '#f0f0f0',
    paddingHorizontal: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontWeight: 'bold',
    marginRight: 4,
  },
});
