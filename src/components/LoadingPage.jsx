import React from 'react';
import {PageCenter} from '../styled/views';
import LoadingIcon from './LoadingIcon';

const LoadingPage = () => {
  return (
    <PageCenter>
      <LoadingIcon />
    </PageCenter>
  );
};

export default LoadingPage;
