import axios from 'axios';
const domain = 'https://search.reservamos.mx/api/v2/';
const domainWeather = 'https://api.openweathermap.org/data/2.5/';
const weatherKey = 'a5a47c18197737e8eeca634cd6acb581';

export const axiosApp = axios.create({
  baseURL: `${domain}`,
  headers: {
    'content-type': 'application/json',
  },
  timeout: 5000,
});

export const axiosWeather = axios.create({
  baseURL: `${domainWeather}`,
  headers: {
    'content-type': 'application/json',
  },
  timeout: 5000,
});
axiosWeather.interceptors.request.use(async req => {
  req.params = {appid: weatherKey, units: 'metric', ...req.params};
  return req;
});
