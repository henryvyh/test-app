import styled from 'styled-components';

// view
export const Flex = styled.View`
  flex: 1;
`;
export const Column = styled.View`
  flex-direction: column;
`;
export const ColumnCenter = styled.View`
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
export const Page = styled.View`
  flex: 1;
  flex-direction: column;
`;
export const PageCenter = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
export const PageScroll = styled.ScrollView`
  flex: 1;
  flex-direction: column;
`;
export const FlexRow = styled.View`
  flex-direction: row;
  flex: 1;
  align-items: center;
`;
export const FlexColumn = styled.View`
  flex-direction: column;
  flex: 1;
`;
export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
export const RowCenter = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;
export const Absolute = styled.View`
  position: absolute;
  top: 0;
  left: 0;
`;
export const Relative = styled.View`
  position: relative;
`;
export const Separator = styled.View`
  width: ${props => props.width ?? 0}px;
  height: ${props => props.height ?? 0}px;
`;
