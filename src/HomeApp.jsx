import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, RefreshControl, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import CardPlace from './components/CardPlace';
import LoadingList from './components/LoadingList';
import MessageView from './components/MessageView';
import SearchInput from './components/SearchInput';
import ToolBarApp from './components/ToolBarApp';
import {searchPlaces} from './redux/place/PlaceActions';
import {PLACE_TYPE} from './redux/place/PlaceTypes';
import {RouterPaths} from './router/RouterPaths';

const HomeApp = props => {
  let {
    route: {params},
    navigation,
  } = props;
  const dispatch = useDispatch();
  const {places, filter} = useSelector(reducers => reducers.placeReducer);
  const [loading, setLoading] = useState(true);
  const [query, setQuery] = useState('');
  const [reload, setReload] = useState(false);
  useEffect(() => {
    callbackInitialData();
  }, []);

  const renderItem = useCallback(
    ({item}) => <CardPlace data={item} onPress={onDetail} filter={filter} />,
    [filter],
  );
  const callbackInitialData = async () => {
    await onSearchPlace('');
    setLoading(false);
  };
  const onSearchPlace = async e => {
    await searchPlaces(dispatch, {q: e?.toLowerCase()?.trim()});
    setQuery(e);
  };
  const onRefreshLocal = async () => {
    setReload(true);
    await onSearchPlace('');
    setReload(false);
  };

  const onDetail = e => {
    navigation.navigate(RouterPaths.comparator, {data: e, filter});
  };
  const onFilter = e => {
    dispatch({
      type: PLACE_TYPE.SET_FILTER,
      payload: e,
    });
  };
  return (
    <>
      <ToolBarApp />
      <SearchInput
        filter={filter}
        defaultValue={query}
        placeholder={'Busca tu nuevo destino'}
        onSearch={onSearchPlace}
        onFilter={onFilter}
      />
      {loading ? (
        <LoadingList />
      ) : (
        <>
          <FlatList
            style={styles.body}
            refreshControl={
              <RefreshControl refreshing={reload} onRefresh={onRefreshLocal} />
            }
            data={places}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            ListEmptyComponent={() => (
              <MessageView
                query={query}
                icon={require('./assets/lottie/search.json')}
                title="Empieza a buscar tu nuevo destino"
                message={
                  'Con información meteorologica para que tu desicion sea la mejor'
                }
              />
            )}
          />
        </>
      )}
    </>
  );
};

export default HomeApp;
const styles = StyleSheet.create({
  body: {
    paddingVertical: 8,
    paddingHorizontal: 8,
  },
});
