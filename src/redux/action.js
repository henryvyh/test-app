import {axiosApp} from '../config/axios';
import {PLACE_TYPE} from './place/PlaceTypes';

export const getAction = async (url, dispatch, type, params) => {
  return await axiosApp({
    url,
    method: 'get',
    params,
  })
    .then(async data => {
      const resData = data?.data;
      await dispatch({
        type: PLACE_TYPE.SET_STORE_PLACE,
        payload: resData,
      });
      return await dispatch({
        type,
        payload: resData,
      });
    })
    .catch(e => {
      return null;
    });
};
