import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import placeReducer from './place/PlaceReducer';

const middleWare = [thunk];
const rootReducer = combineReducers({
  placeReducer,
});
const store = createStore(rootReducer, applyMiddleware(...middleWare));
export default store;
