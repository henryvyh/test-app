import {axiosWeather} from '../../config/axios';
import {getAction} from '../action';
import {PLACE_TYPE} from './PlaceTypes';

export const searchPlaces = async (dispatch, query) =>
  await getAction('places', dispatch, PLACE_TYPE.SET_PLACE, query);

export const searchWeather = async (lat, lon, callback) =>
  await axiosWeather({
    url: 'onecall',
    method: 'get',
    params: {lat, lon},
  })
    .then(async data => {
      const resData = data?.data;
      if (!resData) return;
      callback(resData);
    })
    .catch(e => {
      return null;
    });
