import {PLACE_TYPE} from './PlaceTypes';
export const filtersType = [
  {
    id: 'city',
    icon: 'home-city-outline',
    label: 'City',
  },
  {
    id: 'airport',
    icon: 'airplane',
    label: 'Airport',
  },
  {
    id: 'terminal',
    icon: 'bus-stop',
    label: 'Terminal',
  },
];
const initialState = {
  places: [],
  storePlaces: [],
  filter: filtersType[0],
};
export default function placeReducer(state = initialState, action) {
  switch (action.type) {
    case PLACE_TYPE.SET_STORE_PLACE:
      return {
        ...state,
        storePlaces: action.payload,
      };
    case PLACE_TYPE.SET_PLACE:
      return {
        ...state,
        places: action.payload?.filter(e => e.result_type === state.filter.id),
      };
    case PLACE_TYPE.SET_FILTER:
      return {
        ...state,
        filter: action.payload,
        places: state.storePlaces?.filter(
          e => e.result_type === action.payload.id,
        ),
      };

    default:
      return state;
  }
}
