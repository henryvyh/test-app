import EncryptedStorage from 'react-native-encrypted-storage';
class StoreData {
  static async set(key, value, obj = true) {
    try {
      let _val = obj ? JSON.stringify(value) : value;
      await EncryptedStorage.setItem(key, _val);
    } catch (e) {}
  }
  static async get(key, obj = true) {
    try {
      let result = await EncryptedStorage.getItem(key);
      let _res = obj ? JSON.parse(result) : result;
      return _res;
    } catch (e) {
      return null;
    }
  }
  static async delete(key) {
    try {
      await EncryptedStorage.removeItem(key);
    } catch (e) {}
  }
  static async clear() {
    try {
      await EncryptedStorage.clear();
    } catch (e) {}
  }
  //
  static async getUser() {
    try {
      return await StoreData.get('auth');
    } catch (error) {
      return null;
    }
  }
  static async setUser(obj) {
    try {
      await StoreData.set('auth', obj);
    } catch (error) {
      return null;
    }
  }
  //
}
export default StoreData;
