import {Alert} from 'react-native';
import Share from 'react-native-share';

import ToastMessage from 'react-native-simple-toast';
import {openLink} from './utils';
export const AlertResponse = {
  YES: 'YES',
  NO: 'NO',
  DISMISS: 'DISMISS',
};
export const AlertConfirm = async (title, message) =>
  new Promise(resolve => {
    Alert.alert(
      title ?? 'Tómate un respiro',
      message ?? '¿Estás seguro de completar la acción?',
      [
        {
          text: 'Aceptar',
          onPress: () => {
            resolve(AlertResponse.YES);
          },
        },
        {
          text: 'Cancelar',
          onPress: () => {
            resolve(AlertResponse.NO);
          },
        },
      ],
      {cancelable: true, onDismiss: () => resolve(AlertResponse.DISMISS)},
    );
  });
export const showToast = msg => {
  if (typeof msg !== 'string') {
    inValidToast();
    return;
  }
  ToastMessage.show(msg);
};
const inValidToast = () =>
  ToastMessage.show('Invalid message', ToastMessage.SHORT, ToastMessage.BOTTOM);

export const noServiceApi = () => {
  showToast(
    'Service not found, please try later or contact your administrator',
  );
};
export const openNavigationMap = (lat, lng) => {
  let _url = getUrlLocation(lat, lng);
  openLink(_url);
};
export const getUrlLocation = (lat, lng) =>
  `https://www.google.com/maps/search/?api=1&query=${lat},${lng}`;

export const onShare = async obj => {
  try {
    const result = await Share.open(obj);
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    console.log(error.message);
  }
};
