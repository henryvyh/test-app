import React, {useEffect, useState} from 'react';
import {FlatList, RefreshControl, StyleSheet, View} from 'react-native';
import {Appbar, Headline} from 'react-native-paper';
import {COLORS} from './assets/colors';
import ButtonCompare from './components/ButtonCompare';
import CardDay from './components/CardDay';
import CardDetail from './components/CardDetail';
import CardHour from './components/CardHour';
import LoadingPage from './components/LoadingPage';
import {searchWeather} from './redux/place/PlaceActions';
import {Page, PageScroll, Separator} from './styled/views';
import {openNavigationMap, showToast} from './utils/ui';

const ComparatorApp = props => {
  let {
    route: {params},
    navigation,
  } = props;
  const {data, filter} = params;
  const [dataView, setDataView] = useState({});
  const [loading, setLoading] = useState(true);
  const [reload, setReload] = useState(false);
  const goBack = () => navigation.goBack();

  useEffect(() => {
    callbackInitialData();
  }, []);

  const callbackInitialData = async () => {
    await searchWeather(data.lat, data.long, setDataView);
    setLoading(false);
  };

  const renderItemHour = ({item}) => <CardHour data={item} />;
  const renderItemDay = ({item}) => <CardDay data={item} />;
  const onRefreshLocal = async () => {
    setReload(true);
    await callbackInitialData();
    setReload(false);
  };
  const onReservation = () => {
    showToast('Thank you');
    goBack();
  };
  return (
    <Page>
      <Appbar.Header dark={true}>
        <Appbar.BackAction onPress={goBack} />
        <Appbar.Content
          title={data?.city_name + ' - ' + filter?.label}
          subtitle={data?.country + ' - ' + data?.state}
        />
        <Appbar.Action
          icon="directions"
          onPress={() => openNavigationMap(data.lat, data.long)}
        />
        <Appbar.Action
          icon="dots-vertical"
          onPress={() => console.log('Pressed mail')}
        />
      </Appbar.Header>

      {loading ? (
        <LoadingPage />
      ) : (
        <Page>
          <PageScroll
            refreshControl={
              <RefreshControl refreshing={reload} onRefresh={onRefreshLocal} />
            }
            style={styles.body}>
            <CardDetail data={dataView?.current} />
            <Headline>Hourly</Headline>
            <FlatList
              data={dataView?.hourly}
              horizontal
              renderItem={renderItemHour}
              keyExtractor={item => item.dt}
              ItemSeparatorComponent={() => <Separator width={8} />}
            />
            <Headline>Forecasts</Headline>
            <FlatList
              data={dataView?.daily}
              horizontal
              renderItem={renderItemDay}
              keyExtractor={item => item.dt}
              ItemSeparatorComponent={() => <Separator width={8} />}
            />
          </PageScroll>
          <ButtonCompare loading={reload} onPress={onReservation} />
        </Page>
      )}
    </Page>
  );
};

export default ComparatorApp;
const styles = StyleSheet.create({
  body: {
    flex: 1,
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  viewTemp: {
    paddingVertical: 16,
    elevation: 1,
    marginTop: 8,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTempCurrent: {
    fontSize: 50,
    color: COLORS.primary,
    fontWeight: 'bold',
  },
  hour: {
    fontSize: 25,
    color: COLORS.primary,
  },
  iconCurrent: {
    fontSize: 90,
  },
  icon: {
    color: COLORS.primary,
    fontSize: 30,
    fontFamily: 'WeatherIcons-Regular',
    padding: 0,
  },
});
