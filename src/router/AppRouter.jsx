import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {RouterPaths} from './RouterPaths';
import HomeApp from '../HomeApp';
import ComparatorApp from '../ComparatorApp';
import LoadingPage from '../components/LoadingPage';
import {StatusBar} from 'react-native';
import {COLORS} from '../assets/colors';

const Stack = createNativeStackNavigator();

const AppRouter = () => {
  return (
    <>
      <StatusBar backgroundColor={COLORS.primary} />
      <NavigationContainer fallback={<LoadingPage />}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName={RouterPaths.home}>
          <Stack.Screen name={RouterPaths.home} component={HomeApp} />
          <Stack.Screen
            name={RouterPaths.comparator}
            component={ComparatorApp}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default AppRouter;
