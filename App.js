/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React from 'react';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {COLORS} from './src/assets/colors';
import store from './src/redux/redux';
import {Provider} from 'react-redux';
import AppRouter from './src/router/AppRouter';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: COLORS.primary,
    accent: COLORS.accent,
  },
};
const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <AppRouter />
      </PaperProvider>
    </Provider>
  );
};
export default App;
